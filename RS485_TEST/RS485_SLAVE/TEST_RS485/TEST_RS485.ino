
#include <wiring_private.h> // pinPeripheral() function


/*
int BLE_RESET = 5;

int BLE_control = 26;

Uart nrfUart( &sercom1, 0, 1, SERCOM_RX_PAD_1, UART_TX_PAD_0 ) ;

void SERCOM1_0_Handler()
{
  nrfUart.IrqHandler();
}
void SERCOM1_1_Handler()
{
  nrfUart.IrqHandler();
}
void SERCOM1_2_Handler()
{
  nrfUart.IrqHandler();
}
void SERCOM1_3_Handler()
{
  nrfUart.IrqHandler();
}
*/
int  RS485_IN  = 43 ; //PA08    Sercom 0.0 / 2.1 (HIGH= Sending)/ (LOW= Receiving)
int  RS485_DI  = 44 ; //PA09    Sercom 0.1 / 2.0 (DI_peripheral)
int  RS485_RO  = 45 ; //PA10     Sercom 0.2 / 2.2 (MCU_RO_peripheral(READ_OUTPUT))

Uart LUNA_S( &sercom2, RS485_RO, RS485_DI, SERCOM_RX_PAD_2, UART_TX_PAD_0 ) ;

void SERCOM2_0_Handler()
{
  LUNA_S.IrqHandler();
}
void SERCOM2_1_Handler()
{
  LUNA_S.IrqHandler();
}
void SERCOM2_2_Handler()
{
  LUNA_S.IrqHandler();
}
void SERCOM2_3_Handler()
{
  LUNA_S.IrqHandler();
}

void setup() {
  
  Serial.begin(115200);
  //Serial1.begin(9600);
  //pinMode(BLE_RESET,OUTPUT);
  //nrfUart.begin(115200);
  LUNA_S.begin(115200);

 /* pinPeripheral(1, PIO_SERCOM);
  pinPeripheral(0, PIO_SERCOM);*/
  pinPeripheral(RS485_DI, PIO_SERCOM_ALT);
  pinPeripheral(RS485_RO, PIO_SERCOM_ALT);

    //pinMode(BLE_control,OUTPUT);
   pinMode(RS485_IN,OUTPUT);
   digitalWrite(RS485_IN,LOW);
    
   // digitalWrite(BLE_RESET,HIGH);
   // digitalWrite(BLE_control,LOW);
    
}

void loop() {
  if (LUNA_S.available())
  {
   digitalWrite(RS485_IN,LOW);
    char buf[200];
    LUNA_S.readBytes(buf,200);
    Serial.print(buf);
    
    }
  if (Serial.available()){
    //digitalWrite(DE_RE_peripheral,HIGH);
    LUNA_S.write(Serial.read());
   digitalWrite(RS485_IN,LOW);

    
  }
}
