

int PORTB_D1 = 49;
int ENABLE_B = 48;

int PORTA_D1 = 24;
int ENABLE_A = 41;

void setup() {
  // put your setup code here, to run once:
pinMode(ENABLE_B,OUTPUT);
digitalWrite(ENABLE_B,LOW);
pinMode(PORTB_D1, OUTPUT);
digitalWrite(PORTB_D1,LOW);
pinMode(ENABLE_A,OUTPUT);
digitalWrite(ENABLE_A,LOW);
pinMode(PORTA_D1, OUTPUT);
digitalWrite(PORTA_D1,HIGH);
}

void loop() {
 digitalWrite(ENABLE_B,LOW);
 digitalWrite(PORTB_D1,LOW);
 digitalWrite(PORTA_D1,HIGH);

}
