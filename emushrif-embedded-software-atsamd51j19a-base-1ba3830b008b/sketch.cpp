/*
 * atsamd51-test.cpp
 *
 * Created: 21/09/2020 10:15:50 AM
 * Author : pc
 */ 

#include <Arduino.h>
#include <pinout/gpio-pin-map.h>
#include "samd51j19a.h"
#include <Wire.h>
#include "wiring_private.h"


//a simple sketch. a USB repeater

// pushbutton variable 
//#define  PushButton PreA_IO2_MCU
int PushButtonState = 1 ;
//pir variable 
#define PIR_Sensor1 PreC_IO1_MCU
#define PIR_Sensor2 PreC_IO2_MCU

int PIRSensorState1;
int PIRSensorState2;

// BLE Variable 	
#define BLE_RX nRF_MCU
#define BLE_TX MCU_nRF
Uart nrfUart( &sercom0, BLE_RX, BLE_TX, SERCOM_RX_PAD_1, UART_TX_PAD_0 ) ;

void SERCOM0_0_Handler()
{
	nrfUart.IrqHandler();
}
void SERCOM0_1_Handler()
{
	nrfUart.IrqHandler();
}
void SERCOM0_2_Handler()
{
	nrfUart.IrqHandler();
}
void SERCOM0_3_Handler()
{
	nrfUart.IrqHandler();
}
// I2C 
#define SDA_Pin PreA_IO1_MCU
#define SCL_Pin PreA_IO2_MCU
TwoWire mywire(&sercom2, SDA_Pin, SCL_Pin);





#if 1 //this can disable compiling this file

void setup(void)
{
	/* serial setup*/
	Serial.begin(9600);
	/*i2c set up*/
	mywire.begin();
	Serial.println("\nI2C Scanner");
	pinPeripheral(SDA_Pin, PIO_SERCOM);
	pinPeripheral(SCL_Pin, PIO_SERCOM);
	
	pinMode(PerA_EN, OUTPUT); // Perphiral A
	digitalWrite(PerA_EN, LOW);
	pinMode(PerB_EN, OUTPUT); // Perphiral B
	digitalWrite(PerB_EN, HIGH);
	pinMode(PerC_EN, OUTPUT); // Perphiral C
	digitalWrite(PerC_EN, LOW);
	
	
	 /* BLE setup */
	pinMode(nRF_RESET,OUTPUT);
	nrfUart.begin(115200);
	pinPeripheral(BLE_RX, PIO_SERCOM_ALT);
	pinPeripheral(BLE_TX, PIO_SERCOM_ALT);
	pinMode(BLE_ENBLE,OUTPUT);
	digitalWrite(nRF_RESET,HIGH);
	digitalWrite(BLE_ENBLE,LOW);
	 /* PushButton setup*/
	digitalWrite(PerA_EN,LOW);
	//pinMode( PushButton , INPUT);
	/*PIR SENSOR SETUP*/
	pinMode(PIR_Sensor1, INPUT);
	pinMode(PIR_Sensor2, INPUT);

	
}

void loop(void)
{
	Serial.println("Scanning...");
/*
byte error, address;
int nDevices;

Serial.println("Scanning...");
 

nDevices = 0;
for(address = 1; address < 127; address++ )
{
	// The i2c_scanner uses the return value of
	// the Write.endTransmisstion to see if
	// a device did acknowledge to the address.
	mywire.beginTransmission(address);
	error = mywire.endTransmission();

	if (error == 0)
	{
		Serial.print("I2C device found at address 0x");
		if (address<16)
		Serial.print("0");
		Serial.print(address,HEX);
		Serial.println("  !");

		nDevices++;
	}
	else if (error==4)
	{
		Serial.print("Unknown error at address 0x");
		if (address<16)
		Serial.print("0");
		Serial.println(address,HEX);
	}
}

if (nDevices == 0){
Serial.println("No I2C devices found\n");
}
else{
Serial.println("done\n");
}
delay(5000);*/
	}
#endif