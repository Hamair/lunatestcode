Arduino Core modified by Adafruit for SAMD devices.
This repository is cloned from original GitHub repo: 
https://github.com/adafruit/ArduinoCore-samd 
It does not include bootloaders, drivers and other directories.