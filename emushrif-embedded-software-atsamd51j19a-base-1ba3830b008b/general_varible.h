/*
 * general_varible.h
 *
 * Created: 5/19/2021 10:57:48 AM
 *  Author: pc
 */ /*
#include <Arduino.h>
#include <pinout/gpio-pin-map.h>
#include "wiring_private.h"
#include "samd51g19a.h"
#include <../Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include "elapsedMillis.h"

#ifndef GENERAL_VARIBLE_H_
#define GENERAL_VARIBLE_H_*/
//#error "sketch.cpp is running as your main program. (comment this line out if you want to run it)"
/*********************************************************************************************************/
/*                                   variable                                                            */
/*********************************************************************************************************/
/*
// push button 
#define PushButton    PreA_IO2_MCU
#define PushButtonLED PreA_IO1_MCU	

//RGB indicator
#define LEDS_TOTAL 1
extern Adafruit_NeoPixel strip;

#define LED_OFF				0
#define LED_RED				1
#define LED_GREEN			2
#define LED_WHITE			3
#define LED_YELLOW			4
#define LED_ORANGE			5
#define LED_BLUE			6
#define LED_PURPLE			7
#define LED_LIME			8
#define LED_CYAN			9
#define LED_PINK			10
#define LED_EMUSHRIF_ORANGE 11
#define LED_TURQUOISE		12


#define LED_ERROR		LED_RED
#define LED_SUCCESS		LED_GREEN
#define LED_ALL			LED_WHITE
#define LED_RS_SEND		LED_YELLOW
#define LED_RS_REC		LED_ORANGE
#define LED_BLE_SEND	LED_BLUE
#define LED_BLE_REC		LED_PURPLE
#define LED_SBC_SEND	LED_LIME
#define LED_SBC_REC		LED_CYAN
// buzzer
#define BuzzerControl BUZZER
typedef elapsedMillis stopwatch;

// alarm 

#define Alarm  PerB_EN

// PIR sensor 
#define PIR_Sensor PreC_IO2_MCU

// safety function variable
extern bool safetyTrigger;
*/
/**************************************************************************************************************/
/*                                BLE variable and sercom                                                     */
/**************************************************************************************************************/
/*
//BLE MODULE
#define BLE_EN		BLE_ENBLE
#define BLE_RX		nRF_MCU
#define BLE_TX		MCU_nRF
#define BLE_RESET	nRF_RESET
//sercom1 used to read the BLE tags
extern Uart nrfUart;

extern char BLE_Detect[12288];

*/
/***************************************************************************************/
/*      communication between the space board and Luna using the RS485                 */
/***************************************************************************************/
/*
//RS485 PIN Config
#define RS485_EN		RS485_IN //Active LOW
#define RS_TX			RS485_DI
#define RS_RX			RS485_RO
#define RS_READWRITE	RS485_IN
#define RS_SEND			HIGH
#define RS_RECEIVE		LOW
// sercom 2 used for the communication between the space and Luna
extern Uart rsUart;
#endif /* GENERAL_VARIBLE_H_ */*/