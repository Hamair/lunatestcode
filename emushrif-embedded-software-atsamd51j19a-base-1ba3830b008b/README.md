# README #

Base project for ATSAMD51J19A.
Based on Adafruit Metro M4 board.
To use this base clone it then push it into a new remote (empty) repository.


The template includes the project file (.cppproj) with Adafruit Arduino Core . In addition it includes an examples sketch (sketch.cpp). If you want to run the sketch, you should comment-out a line that starts with #error(it’s there to prevent you from adding your own code and running sketch instead).

Template also has global.h. This file is included globally. Any definitions/includes in this file will be included by compiler automatically.

The default include search directories are CMSIS directory (used to interface the program to hardware registers), Arduino Core directory and the top-level directory (which has sketch.cpp and global.h).

**Selecting Linker**
Project is setup to work with 32KiB rocket bootloader. Linker file for 16KiB bootloader is also available. They are all under `linker/` directory. You can select linker from project properties > toolchain > ARM/GNU Linker > Miscellaneous.

**Naming Your Project**

After downloading the template, rename the .cppproj file to your project’s name then open it (in Microchip Studio).

Then go to Project → Properties.

Choose “Build” from the left pane.

Change “Configuration” to be “Debug”.

In the same page, change “Artifact Name” to the name of your project.

Change “Configuration” to be “Release”.

Again, in the same page, change “Artifact Name” to the name of your project.

Now the project file and output files will have the proper name.


**Updating Arduino Core**

It is recommended to update Arduino core to make sure you have the latest version.

You can add the core exists as a subtree in the template folder. You can link it to it’s corresponding repository (in Sourcetree) then pull changes. Or run git command:

    git subtree pull -P adafruit-arduinocore-samd <repository>

repository (if not changed) should be

    <bitbucket_username>@bitbucket.org/emushrif-embedded-software/adafruit-arduinocore-samd.git