/*
 * gpio_pin_map.h
 * Pin mapping to use Arduino Libraries
 * Created: 23/09/2020 2:42:56 PM
 *  Author: pc
 */ 

#define LED_IC				8   //PA21
#define BUZZER				46  //PA12

#define PerA_EN				41  //PB10	
#define PreA_IO1_MCU		26  //PA12	
#define PreA_IO2_MCU		25  //PA13	
#define PreA_OD1_MCU		24  //PA14	
#define PreA_OD2_MCU		6   //PA15	

#define PerB_EN				2   // PB31	
#define PreB_IO1_MCU		0   //PA22	
#define PreB_IO2_MCU		1   //PA23	
#define PreB_IO3_MCU		9   //PA20	
#define PreB_OD1_MCU		3   //PB23	


#define PerC_EN				4   //PB30	
#define PreC_IO1_MCU		20  //PB02	
#define PreC_IO2_MCU		21  //PB03	

#define BLE_ENBLE			29  //PA07	 BLE_control
#define MCU_nRF				17  //PA04  BLE_TX Sercom 0.0
#define nRF_MCU				15  //PA05   BLE_RX Sercom 0.1
#define nRF_RESET			19  //PB09


#define TRG_USB_N			30  //PA24	Sercom 3.3 / 5.3
#define TRG_USB_P			31  //PA25	Sercom 3.2 / 5.2


#define RS485_IN			43  //PA08		Sercom 0.0 / 2.1 (HIGH= Sending)/ (LOW= Receiving)
#define RS485_DI			44  //PA09 		Sercom 0.1 / 2.0 (DI_peripheral)
#define RS485_RO			45  //PA10 		Sercom 0.2 / 2.2 (MCU_RO_peripheral(READ_OUTPUT))

