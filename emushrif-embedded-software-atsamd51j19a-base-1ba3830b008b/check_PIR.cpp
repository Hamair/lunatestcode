/*
 * check_PIR.cpp
 *
 * Created: 6/16/2021 10:07:01 AM
 *  Author: Humair Alatbi
 */ 
/*
#include <Arduino.h>
#include <pinout/gpio-pin-map.h>
#include <../Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include "general_varible.h"
#include "writeReadFromSpace.h"
#include "elapsedMillis.h"
#include "WString.h"
#include "checkPushButton.h"
#include "sound_Indicator.h"


int PIRSensorState{0};
int	PIRStatePrevious{0};

bool PIRStateDetect =false;

const int intervalPIR =50;	
unsigned long  PIR_Detect_Time;              // time pressed the pushButton
unsigned long  latestMotinDetect;				//the latest reading of the pushButton
elapsedMillis PIRCurrentTime;				//time between the two reading of the PIR Sensor 

void check_PIR(String check_operation ){
	if(check_operation.compareTo("PIR_ON")==0){
		digitalWrite(PerC_EN, LOW);
		//sendTospace("PIR_ON_RECEIVE");
			Serial.println("PIR_ON");
			PIRSensorState= digitalRead(PIR_Sensor);
			Serial.println(PIRSensorState);
			PIRCurrentTime =0;
		while(PIRCurrentTime<120000){
			//Serial.println("I AM IN THE while loop ");
			PIRSensorState= digitalRead(PIR_Sensor);
			if(PIRSensorState== HIGH){
				PIR_Detect_Time = PIRCurrentTime;
				Serial.println("Motion is detected");
				//sendTospace("Motion is detected");
				//soundIndicator("BUZZER_Beep2");
				char last_time_PIR_Detect[20]={0};
				Serial.print("PIR_TIME: ");
				//sendTospace("PIR_TIME: ");
				itoa(PIR_Detect_Time,last_time_PIR_Detect,10);
				Serial.println(last_time_PIR_Detect);
				//sendTospace(last_time_PIR_Detect);
			}	
			
			
			}// end pf the while loop 
			
			Serial.println("out of the detection loop");
			//sendTospace("out of the detection loop");

	}//PIR_ON End
	else if(check_operation.compareTo("PIR_OFF")==0){
		Serial.println("PIR_OFF");
		//sendTospace("PIR_OFF_RECEIVE");
		digitalWrite(PerC_EN, HIGH);
	}
	else{
		//sendTospace("invalid massage (PIR)");
		Serial.println("invalid massage (PIR)");
	}
	
}// function end 

*/