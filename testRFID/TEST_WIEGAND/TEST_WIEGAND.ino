#include <Wiegand.h>



int PORTA_IO2 = 50;
int PORTA_IO1 = 51;
int ENABLE_A = 41;
int PORTA_D1 = 24;
int PORTA_D2 = 52;


WIEGAND wg;

void setup() {
  Serial.begin(9600);  
  pinMode(ENABLE_A,OUTPUT);
  digitalWrite(ENABLE_A,LOW);
//  pinMode( PORTA_IO1 , INPUT);
//  pinMode( PORTA_IO2 , INPUT);
pinMode(PORTA_D1, OUTPUT);
//digitalWrite(PORTA_D1,HIGH);
pinMode(PORTA_D2, OUTPUT);
//digitalWrite(PORTA_D2,HIGH);
  // default Wiegand Pin 2 and Pin 3 see image on README.md
  // for non UNO board, use wg.begin(pinD0, pinD1) where pinD0 and pinD1 
  // are the pins connected to D0 and D1 of wiegand reader respectively.
  wg.begin(PORTA_IO1,PORTA_IO2);
}

void loop() {
  if(wg.available())
  {
    Serial.print("Wiegand HEX = ");
    Serial.print(wg.getCode(),HEX);
    Serial.print(", DECIMAL = ");
    Serial.print(wg.getCode());
    Serial.print(", Type W");
    Serial.println(wg.getWiegandType()); 
    digitalWrite(PORTA_D1,HIGH);
   digitalWrite(PORTA_D2,HIGH);
   delay(500);
   digitalWrite(PORTA_D1,LOW);
   digitalWrite(PORTA_D2,LOW);
  }
}
