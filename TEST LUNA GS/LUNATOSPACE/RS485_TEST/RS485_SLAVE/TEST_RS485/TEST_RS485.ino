//space to luna 
#include <wiring_private.h> // pinPeripheral() function


/*
int BLE_RESET = 5;

int BLE_control = 26;

Uart nrfUart( &sercom1, 0, 1, SERCOM_RX_PAD_1, UART_TX_PAD_0 ) ;

void SERCOM1_0_Handler()
{
  nrfUart.IrqHandler();
}
void SERCOM1_1_Handler()
{
  nrfUart.IrqHandler();
}
void SERCOM1_2_Handler()
{
  nrfUart.IrqHandler();
}
void SERCOM1_3_Handler()
{
  nrfUart.IrqHandler();
}
*/


/* enable 5v */ 
int V5_Enable =27; 

/*   setup uart  */ 
int  RS485_IN  = 24 ; //PA14    Sercom 0.0 / 2.1 (HIGH= Sending)/ (LOW= Receiving)
int  RS485_DI  = 3 ; //BP16    Sercom 0.1 / 2.0 (DI_peripheral)
int  RS485_RO  = 2 ; //PB17     Sercom 0.2 / 2.2 (MCU_RO_peripheral(READ_OUTPUT))

Uart Space_S( &sercom5, RS485_RO, RS485_DI, SERCOM_RX_PAD_1, UART_TX_PAD_0 ) ;

void SERCOM5_0_Handler()
{
  Space_S.IrqHandler();
}
void SERCOM5_1_Handler()
{
  Space_S.IrqHandler();
}
void SERCOM5_2_Handler()
{
  Space_S.IrqHandler();
}
void SERCOM5_3_Handler()
{
  Space_S.IrqHandler();
}

void setup() {
  // FORE POWER ENABLE 
  //pinMode(12V_Enable,OUTPUT);
//  digitalWrite(12V_Enable,LOW);
  pinMode(V5_Enable,OUTPUT);
  digitalWrite(V5_Enable,LOW);  
  
  Serial.begin(115200);
  //Serial1.begin(9600);
  //pinMode(BLE_RESET,OUTPUT);
  //nrfUart.begin(115200);
  Space_S.begin(115200);

 /* pinPeripheral(1, PIO_SERCOM);
  pinPeripheral(0, PIO_SERCOM);*/
  pinPeripheral(RS485_DI, PIO_SERCOM);
  pinPeripheral(RS485_RO, PIO_SERCOM);

    //pinMode(BLE_control,OUTPUT);
   pinMode(RS485_IN,OUTPUT);
   digitalWrite(RS485_IN,LOW);
    
   // digitalWrite(BLE_RESET,HIGH);
   // digitalWrite(BLE_control,LOW);
    
}

void loop() {
  if (Space_S.available())
  {
   digitalWrite(RS485_IN,LOW);
    char buf[200];
    Space_S.readBytes(buf,200);
    Serial.print(buf);
    
    }
  if (Serial.available()){
    //digitalWrite(DE_RE_peripheral,HIGH);
    Space_S.write(Serial.read());
   digitalWrite(RS485_IN,LOW);

    
  }
}
