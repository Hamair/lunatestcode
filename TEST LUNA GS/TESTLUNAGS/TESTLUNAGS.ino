
#include <wiring_private.h> // pinPeripheral() function


/* PIR SETUP*/
int portC_inputSignal_102 = 21;
int detect_signal_102=0;

/*open drain*/

int PORTB_D1 = 49;
int ENABLE_B = 48;

int PORTA_D1 = 24;
int PORTA_D2 = 52;
int ENABLE_A = 41;

/* BLE setup*/
int nRF_RESET = 19;
int BLE_ENBLE = 47;
int  BLE_RX = 15;
int  BLE_TX =  17;

Uart nrfUart( &sercom0, BLE_RX, BLE_TX, SERCOM_RX_PAD_1, UART_TX_PAD_0 ) ;

void SERCOM0_0_Handler()
{
  nrfUart.IrqHandler();
}
void SERCOM0_1_Handler()
{ 
  nrfUart.IrqHandler();
}
void SERCOM0_2_Handler()
{
  nrfUart.IrqHandler();
}
void SERCOM0_3_Handler()
{
  nrfUart.IrqHandler();
}

/* rs485   setup*/
int  RS485_IN  = 43 ; //PA08    Sercom 0.0 / 2.1 (HIGH= Sending)/ (LOW= Receiving)
int  RS485_DI  = 44 ; //PA09    Sercom 0.1 / 2.0 (DI_peripheral)
int  RS485_RO  = 45 ; //PA10     Sercom 0.2 / 2.2 (MCU_RO_peripheral(READ_OUTPUT))

Uart LUNA( &sercom2, RS485_RO, RS485_DI, SERCOM_RX_PAD_2, UART_TX_PAD_0 ) ;

void SERCOM2_0_Handler()
{
  LUNA.IrqHandler();
}
void SERCOM2_1_Handler()
{
  LUNA.IrqHandler();
}
void SERCOM2_2_Handler()
{
  LUNA.IrqHandler();
}
void SERCOM2_3_Handler()
{
  LUNA.IrqHandler();
}

void setup() {
 nrfUart.begin(115200);
 LUNA.begin(115200);
 Serial.begin(115200);
  
  //ble 
  pinPeripheral(BLE_RX, PIO_SERCOM_ALT);
  pinPeripheral(BLE_TX, PIO_SERCOM_ALT);
  //slave luna
  pinPeripheral(RS485_DI, PIO_SERCOM_ALT);
  pinPeripheral(RS485_RO, PIO_SERCOM_ALT);

   pinMode(BLE_ENBLE,OUTPUT);
   pinMode(nRF_RESET,OUTPUT);
   digitalWrite(nRF_RESET,HIGH);
   digitalWrite(BLE_ENBLE,LOW);
   pinMode(RS485_IN,OUTPUT);
   digitalWrite(RS485_IN,LOW);

   /*PIR SETUP*/
   pinMode(portC_inputSignal_102, INPUT);
  /* open drain setup*/
    pinMode(ENABLE_B,OUTPUT);
  digitalWrite(ENABLE_B,LOW);
  pinMode(PORTB_D1, OUTPUT);
  digitalWrite(PORTB_D1,LOW);
  pinMode(ENABLE_A,OUTPUT);
  digitalWrite(ENABLE_A,LOW);
  pinMode(PORTA_D1, OUTPUT);
  digitalWrite(PORTA_D1,HIGH);
    pinMode(PORTA_D2, OUTPUT);
  digitalWrite(PORTA_D2,HIGH);
     
    
}

void loop() {
   if (nrfUart.available()) {
      //sending the beacon to the space
      digitalWrite(RS485_IN,HIGH);
      char BLE_Detect[200];
      nrfUart.readBytes(BLE_Detect,200);
      LUNA.print(BLE_Detect);
      //Serial.print(BLE_Detect);
      digitalWrite(RS485_IN,LOW);
    }
     if (Serial.available()){
    digitalWrite(RS485_IN,HIGH);
    char c = Serial.read();
    char x= nrfUart.write(c);
    LUNA.write(c);
   // Serial.println(x);
  }
  // PIR TEST//
  detect_signal_102 = digitalRead(portC_inputSignal_102);
  Serial.print("detect_signal_102 = ");
  Serial.println(detect_signal_102) ;
  delay(10);
  //test open drain 
 digitalWrite(PORTB_D1,HIGH);
 digitalWrite(PORTA_D1,HIGH);
 digitalWrite(PORTA_D2,HIGH);

  
  
}
