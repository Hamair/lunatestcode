	/*
	* atsamd51-test.cpp
	*
	* Created: 21/09/2020 10:15:50 AM
	* Author : pc
	*/

	/*
	LUNA:
	Arthur : Humair Alatbi
	date   : 5/5/2021
	project: In_Bus_Test

	other information:
	this code written for Luna Board (peripheral board) which formed as a slave broad with space(main Board) master Board.

	*/

/**********************************************************************************************************/
/*                                     included library                                                   */
/**********************************************************************************************************/
#include <Arduino.h>
#include <pinout/gpio-pin-map.h>
#include "samd51g19a.h"
#include <../Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include "wiring_private.h"
#include "general_varible.h"
#include "BLE_Scaner.h"
#include "writeReadFromSpace.h"
#include "buzzer.cpp"
#include "LED_Indicator.h"
#include "elapsedMillis.h"
#include "check_IGN.h"
#include "checkPushButton.h"
#include "sound_Indicator.h"
#include "check_PIR.h"


extern Uart rsUart;

#if 1 //this can disable compiling this file
	//#error "sketch.cpp is running as your main program. (comment this line out if you want to run it)"
String ledString = "1";
int ledValue = 0;
void setup(void){
	
	Serial.begin(9600);
		
	/****************************** Regulators (Enable port) ******************************/
	strip.show();
	strip.setPixelColor(0, strip.Color(200,200,200));
	delay(250);
	pinMode(PerA_EN, OUTPUT); // Perphiral A
	digitalWrite(PerA_EN, LOW);
	pinMode(PerB_EN, OUTPUT); // Perphiral B
	digitalWrite(PerB_EN, HIGH);
	pinMode(PerC_EN, OUTPUT); // Perphiral C
	digitalWrite(PerC_EN, LOW);
		
	/******************************** RGB initializations ********************************/
	strip.begin();
	strip.setBrightness(10);
	strip.show(); // Initialize all pixels to 'off'
	strip.setPixelColor(0, strip.Color(200,0,0));
	delay(250);
		
	/******************************** BLE Serial******************************************/
	strip.show();
	strip.setPixelColor(0, strip.Color(0,0,200));
	delay(250);
	pinMode(BLE_RESET,OUTPUT);
	nrfUart.begin(115200);
	pinPeripheral(BLE_RX, PIO_SERCOM);
	pinPeripheral(BLE_TX, PIO_SERCOM);
	pinMode(BLE_EN,OUTPUT);
	digitalWrite(BLE_RESET,HIGH);
	digitalWrite(BLE_EN,LOW);
	/***************************** RS485 communecation setup ****************************/
	strip.show();
	strip.setPixelColor(0, strip.Color(100,100,0));
	delay(250);
	rsUart.begin(9600);
	pinPeripheral(RS_RX, PIO_SERCOM_ALT);
	pinPeripheral(RS_TX, PIO_SERCOM_ALT);
	pinMode(RS_READWRITE, OUTPUT);
	digitalWrite(RS_READWRITE,RS_RECEIVE);
	/**********************************************buzzer setup************************************************/
	pinMode(BuzzerControl, OUTPUT);
	digitalWrite(BuzzerControl, LOW);
	/*******************************************push button***************************************************/
	pinMode( PushButton , INPUT);
	/****************************************PIR sensor**************************************************/
	pinMode(PIR_Sensor, INPUT);
		
	}

void loop(void)
	{
		
		strip.setBrightness(10);
		strip.show(); // Initialize all pixels to 'off'
		strip.setPixelColor(0, strip.Color(200,0,0));
		delay(250);
		strip.setBrightness(10);
		strip.show(); // Initialize all pixels to 'off'
		strip.setPixelColor(0, strip.Color(0,200,0));
	//String check_operation="";
	//check_operation = recFromSpace();
	////if (rsUart.available()>0){
		////check_operation = rsUart.readString();
	///********************************************* IGN Function check ***************************************/
	//if (check_operation.startsWith("IGN")){
		//IGN_Check(check_operation);
		//Serial.print("IGN: ");
		//Serial.println(check_operation);
		//} // end of the with start IGN
	///********************************************** BL  E Function check ************************************/
		//else if (check_operation.startsWith("BLE")){
			//// call the BLE function
			//BLE_Scaner(check_operation);
			//rsUart.println("BLE MODE");
			//Serial.print("BLE: ");
			//Serial.println(check_operation);
		//}
	///****************************************** start BUZZER Function check ********************************/
		//else if (check_operation.startsWith("BEEP")){
			//soundIndicator(check_operation);
			//Serial.print("BEEP: ");
			//Serial.println(check_operation);
		//}
		///***************************** End of the buzzer check function ************************************/
		//
		///***************************** start checkPushButton Function check ********************************/
		//else if (check_operation.startsWith("PB")){
			//check_pushButton(check_operation);	
			//digitalWrite(PerA_EN, LOW);
		//Serial.print("PushBTN: ");
		//Serial.println(check_operation);
		//}
		///***************************** End of the checkPushButton check function **************************/
	   //
	    ///***************************** start check_PIR Function check *************************************/
		//else if (check_operation.startsWith("PIR")){
		//check_PIR(check_operation);
		//Serial.print("PIR: ");
		//Serial.println(check_operation);
		//}
	    ///***************************** End of the check_PIR check function ********************************/		
			//
		///*************************************** START PING Function test ************************************************/
		//else if  (check_operation.startsWith("PING")){
			//if (check_operation.compareTo("PING")==0){
				//rsUart.println("PING_OK");
				//Serial.println("PING_OK");
				//sendTospace("PING_OK");
			//}
			//else {
				//// send in valid massage
				//sendTospace("NOT a valid massage (PING)");
				//rsUart.println("NOT a valid massage (PING)");
				//Serial.print("Invalid: ");
				//Serial.println(check_operation);
			//}
		//}
		///*************************************** END PING Function test ************************************************/
		//
		///********************************************* LED TEST FUNCTION ***********************************/
		//else if (check_operation.startsWith("LED")){
			 //if (check_operation.startsWith("LED_", 0))
			 //{
				 //ledString = check_operation.substring(4);
				 //ledValue =  ledString.toInt();
				 //ledStatus(ledValue);
				 //} else {
				 //Serial.println("invalid = LED");
			 //}
//
		//}
	///************************************** END LED function *******************************************/	
	////}//SERIAL.AVAILABL 
	//
	}// for the void loop
	#endif