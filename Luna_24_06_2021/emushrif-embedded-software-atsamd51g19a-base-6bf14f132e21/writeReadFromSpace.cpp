/*
* writeReadFromSpace.cpp
*
* Created: 5/19/2021 12:58:22 PM
*  Author: pc
*/

#include <Arduino.h>
#include "general_varible.h"
#include <pinout/gpio-pin-map.h>
#include <../Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include "LED_Indicator.h"
#include "elapsedMillis.h"
#include "WString.h"


/*****************************************************************************************************/
/*                      RS485 >>  communication between Luna and space                               */
/*****************************************************************************************************/
elapsedMillis TimeToReceive;
unsigned long interval = 20000;

void sendTospace(String msg){
	// RS485
	//Sending
	digitalWrite(RS_READWRITE,RS_SEND);
	delay(1);
	rsUart.write(150);
	rsUart.print(msg);
	rsUart.write(169);
	rsUart.flush();
	ledStatus(LED_RS_SEND);
	delay(1);
	digitalWrite(RS_READWRITE,RS_RECEIVE);
	//end of sending 
}

String recFromSpace(){
	String MassageFromSpace="";
	digitalWrite(RS_READWRITE,RS_RECEIVE);
		while (rsUart.available()){
			if (char(150) == rsUart.read()){
				MassageFromSpace = rsUart.readStringUntil(char(169));
				Serial.println(MassageFromSpace);	
			}
			else{
				Serial.println("Not start char!");
		}
	}
	return MassageFromSpace;
}

