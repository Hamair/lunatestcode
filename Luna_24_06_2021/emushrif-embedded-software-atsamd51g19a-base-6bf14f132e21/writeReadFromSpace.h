/*
 * writeReadFromSpace.h
 *
 * Created: 5/19/2021 1:00:20 PM
 *  Author: pc
 */ 

#include <Arduino.h>
#include "wiring_private.h"
#include "general_varible.h"

#ifndef WRITEREADFROMSPACE_H_
#define WRITEREADFROMSPACE_H_

extern void sendTospace(String msg);
extern String recFromSpace();



#endif /* WRITEREADFROMSPACE_H_ */