/*
 * led_indicator.cpp
 *
 * Created: 5/19/2021 2:12:15 PM
 *  Author: pc
 */ 
#include <Arduino.h>
#include <pinout/gpio-pin-map.h>
#include "samd51g19a.h"
#include <../Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include "wiring_private.h"
#include "general_varible.h"

void ledInit(){
	strip.begin();
	strip.setBrightness(50);
	strip.show(); // Initialize all pixels to 'off'
}

void ledStatus(int Status){
	switch (Status){
		case LED_TURQUOISE:
		strip.show();
		strip.setPixelColor(0, strip.Color(0,200,30)); //TURQUOISE
		break;
		
		case LED_PINK:
		strip.show();
		strip.setPixelColor(0, strip.Color(255,80,120)); //PINK
		break;
		
		case LED_EMUSHRIF_ORANGE:
		strip.show();
		strip.setPixelColor(0, strip.Color(255,30,0));
		break;
		
		case LED_RS_SEND:
		strip.show();
		strip.setPixelColor(0, strip.Color(255,255,0)); //YELLOW
		break;
		
		case LED_RS_REC:
		strip.show();
		strip.setPixelColor(0, strip.Color(255,128,0)); //orange
		break;
		
		case LED_SBC_SEND:
		strip.show();
		strip.setPixelColor(0, strip.Color(128,255,0)); //Lime
		break;
		
		case LED_SBC_REC:
		strip.show();
		strip.setPixelColor(0, strip.Color(0,255,255)); //Cyan
		break;
		
		case LED_BLE_SEND:
		strip.show();
		strip.setPixelColor(0, strip.Color(0,0,255)); //Blue
		break;
		
		case LED_BLE_REC:
		strip.show();
		strip.setPixelColor(0, strip.Color(128,0,255));//Purple
		break;
		
		case LED_ERROR:
		strip.show();
		strip.setPixelColor(0, strip.Color(255,0,0)); // Red
		break;
		
		case LED_SUCCESS:
		strip.show();
		strip.setPixelColor(0, strip.Color(0,255,0)); // Green
		break;
		
		case LED_ALL:
		strip.show();
		strip.setPixelColor(0, strip.Color(255,255,255)); // White
		break;
		
		default:
		strip.show();
		strip.setPixelColor(0, strip.Color(255,100,180)); //PINK
		
	}
	delay(100);
	
}