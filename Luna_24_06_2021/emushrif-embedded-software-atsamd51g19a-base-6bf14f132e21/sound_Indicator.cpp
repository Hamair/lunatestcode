/*
 * sound_Indicator.cpp
 *
 * Created: 6/16/2021 12:43:31 AM
 *  Author: Owner
 */ 


#include <Arduino.h>
#include <pinout/gpio-pin-map.h>
#include "samd51g19a.h"
#include <../Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include "wiring_private.h"
#include "general_varible.h"
#include "writeReadFromSpace.h"
#include "buzzer.cpp"
#include "LED_Indicator.h"
#include "checkPushButton.h"

elapsedMillis buzzerTimer;
String valueString = "1000";
int value = 0;
int freq =100;
void beep(int delayTime, int freq){
	pinMode(BUZZER, OUTPUT);
	tone(BUZZER, freq);
	delay(delayTime);
	noTone(BUZZER);
	pinMode(BUZZER, INPUT);
}

void soundIndicator(String check_operation){
	 if (check_operation.startsWith("BEEP_", 0))
	 {
		valueString = check_operation.substring(5);
		value =  valueString.toInt();
		beep(value,freq);
	 } else {
		 Serial.println("invalid = BEEP");
	 }
	/*
	if (check_operation.compareTo("BUZZER_Beep1") == 0 ){
		sendTospace("BUZZER_Beep1");
		Serial.println("BUZZER_Beep1");
		tone(BuzzerControl, 100);
		delay(3000);
		noTone(BuzzerControl);
		ledStatus(LED_SUCCESS);
	}
	else if (check_operation.compareTo("BUZZER_Beep2")==0){
		sendTospace("BUZZER_Beep2");
		Serial.println("BUZZER_Beep2");
		tone(BuzzerControl, 100);
		buzzerTimer = 0;
		while(check_pushButton("PB") == false && buzzerTimer<=120000){
			Serial.println("check push button");
			tone(BuzzerControl, 100);
		}
		Serial.println("BUZZER_Beep2");
		noTone(BuzzerControl);
		ledStatus(LED_SUCCESS);		
		
	}
	else if (check_operation.compareTo("BUZZER_OFF")== 0){
		sendTospace("BUZZER OFF");
		Serial.println("BUZZER OFF");
	}
	else {
		Serial.println("not valid massage!(BUZZER)");
	}
	
	*/
	
	
}


