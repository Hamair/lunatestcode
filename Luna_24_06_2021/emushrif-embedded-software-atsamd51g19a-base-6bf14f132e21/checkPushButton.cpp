/*
 * checkPushButton.cpp
 *
 * Created: 6/15/2021 12:32:22 PM
 *  Author: Humair Alatbi 
 */ 


// library include 
#include <Arduino.h>
#include <pinout/gpio-pin-map.h>
#include <../Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include "general_varible.h"
#include "writeReadFromSpace.h"
#include "elapsedMillis.h"
#include "WString.h"

// variable declaration 
int PushButtonState{1};
int	ButtonStatePrevious{1};
bool ButtonStateLongPress =false;
const int intervalButton =50;					//time between the two reading of the button state
const long MinButtonPressDuration =3000;        // minimum press for the PushButton 3sec
unsigned long ButtonLongPressTime;              // time pressed the pushButton
unsigned long   latestButtonPressed;				//the latest reading of the pushButton 
unsigned long ButtonPressDuration;
elapsedMillis pushButtonCurrentTime;
elapsedMillis pushButtonEndInterval;



bool check_pushButton(String check_operation){
	if (check_operation.compareTo("PB") == 0){
	digitalWrite(PerA_EN, HIGH);
	Serial.println("i AM in the Pb function check");
	pushButtonEndInterval =0;
	while(pushButtonEndInterval<120000){

	PushButtonState = digitalRead(PushButton);// reading the input from the PushButton
	if(PushButtonState== LOW && ButtonStatePrevious==HIGH){
		ButtonLongPressTime = pushButtonCurrentTime;
		ButtonStatePrevious = LOW;
		Serial.println("button is pressed");
		digitalWrite(PerA_EN, LOW);

	}
		ButtonPressDuration = pushButtonCurrentTime-ButtonLongPressTime;
		if(PushButtonState== LOW && ButtonPressDuration>=MinButtonPressDuration){
			ButtonStateLongPress = true;
			Serial.println("button long pressed");
			sendTospace("PB_PRESSED");
			latestButtonPressed = ButtonLongPressTime;
			Serial.print("last pressed: ");
			Serial.println(latestButtonPressed);
			char last_time_pushbutton[20]={0};
			sendTospace("PB_TIME: ");
			itoa(latestButtonPressed,last_time_pushbutton,10);
			sendTospace(last_time_pushbutton);
			pushButtonCurrentTime = 0;
		}
		
		if(PushButtonState==HIGH && ButtonStatePrevious==LOW){
			ButtonStatePrevious = HIGH;
			Serial.println("button released");
			digitalWrite(PerA_EN, HIGH);

			
			if (ButtonPressDuration < MinButtonPressDuration) {
				Serial.println("Button Unpressed");
				sendTospace("Button Unpressed");
				 ButtonStateLongPress=false;
			}
		}
	
	}// while loop for the 2min
	Serial.println("out of the Button loop");
	sendTospace("out of the Button loop");
	}//FOR THE IF check_operation.compareTo("PB")
else{
	Serial.println("not a valid massage (push_Button)");
	sendTospace("not a valid massage (push_Button)");
}
return ButtonStateLongPress;
}