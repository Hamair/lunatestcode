/*
 * BLE_Scaner.cpp
 *
 * Created: 5/19/2021 10:55:41 AM
 *  Author: pc
 */ 

#include <Arduino.h>
#include "general_varible.h"
#include <pinout/gpio-pin-map.h>
#include <../Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include "BLE_Scaner.h"
#include "writeReadFromSpace.h"
#include "LED_Indicator.h"

/***************************************************************************************************/
/*                                    BLE_Scaner function                                          */
/***************************************************************************************************/

extern Uart nrfUart;

void BLE_Scaner(String check_operation){
	
	
	if (check_operation.compareTo("BLE_ON") == 0 ){
		
		digitalWrite(BLE_EN, LOW);
		sendTospace("BLE is On");
		if (nrfUart.available()) {
			// sending the beacon to the space
			char BLE_Detect[SERIAL_BUFFER_SIZE];
			memset(BLE_Detect, '\0', SERIAL_BUFFER_SIZE); //clear buffer
			nrfUart.readBytes(BLE_Detect,nrfUart.available());
			//sendTospace(BLE_Detect);
			Serial.print(BLE_Detect);
			ledStatus(LED_BLE_SEND);
		}
		
	}
	else if( check_operation.compareTo("BLE_OFF") == 0){
		digitalWrite(BLE_EN, HIGH);
		//sendTospace(" BLE TURN OFF");
		Serial.println(" BLE TURN OFF");
		ledStatus(LED_BLE_REC);

		
		
	}
	else {
		//sendTospace(" invalid massage (BLE mode)");
		Serial.println("invalid massage (BLE mode)");
	}
	
	}