//LUNA

#define LED_IC				11  //PB11
#define BUZZER				37  //PA12

#define PerA_EN				32  //PB10	(-)
#define PreA_IO1_MCU		35  //PA09	Sercom 2.0 / 4.1
#define PreA_IO2_MCU		34  //PA08	Sercom 2.2 / 4.2
#define PreA_OD1_MCU		36  //PA10	Sercom 2.3 / 4.3
#define PreA_OD2_MCU		37  //PA11	Sercom 1.0 / 3.1

#define PerB_EN				9  // PA19	(-)
#define PreB_IO1_MCU		13  //PA22	Sercom 3.0 / 5.1
#define PreB_IO2_MCU		12  //PA23	Sercom 3.1 / 5.0
#define PreB_IO3_MCU		10  //PA20	Sercom 3.2 / 5.2
#define PreB_OD1_MCU		11  //PA21	Sercom 1.3 / 5.3


#define PerC_EN				17  //PB09	(-)
#define PreC_IO1_MCU		18  //PA04	Sercom 5.0
#define PreC_IO2_MCU		15  //PA05	Sercom 5.1

#define BLE_ENBLE				26  //PA27	 BLE_control
#define MCU_nRF				0  //PA16   BLE_TX Sercom 0.0
#define nRF_MCU				1  //PA17   BLE_RX Sercom 0.1
#define nRF_RESET			5  //PA15


#define TRG_USB_N			27  //PA24	Sercom 3.3 / 5.3
#define TRG_USB_P			28  //PA25	Sercom 3.2 / 5.2


#define RS485_IN			23  //PA23		Sercom 0.0 / 2.1 (HIGH= Sending)/ (LOW= Receiving)
#define RS485_DI			6  //PB02 		Sercom 0.1 / 2.0 (DI_peripheral)
#define RS485_RO			8  //PB03 		Sercom 0.2 / 2.2 (MCU_RO_peripheral(READ_OUTPUT))

