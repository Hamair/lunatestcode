/*
 * check_IGN.cpp
 *
 * Created: 6/15/2021 1:08:47 PM
 *  Author: Owner
 */ 
#include <Arduino.h>
#include <pinout/gpio-pin-map.h>
#include "samd51g19a.h"
#include <../Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include "wiring_private.h"
#include "general_varible.h"
#include "writeReadFromSpace.h"
#include "buzzer.cpp"
#include "LED_Indicator.h"


void IGN_Check(String check_operation){
	// call the IGN function
	if (check_operation.compareTo("IGN_ON")==0){
		Serial.println("IGN IS ON");
		//sendTospace("IGN_ON_RECEIVE");
		ledStatus(LED_SUCCESS);				}
	else if (check_operation.compareTo("IGN_OFF") == 0){
		
		Serial.println("IGN IS OFF");
		//sendTospace("IGN_OFF_RECEIVE");
		ledStatus(LED_ALL);
	}
	else {
		Serial.println("not valid massage! (IGN mode)");
	}
}