################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

adafruit-arduinoCore-samd\cores\arduino\abi.cpp

adafruit-arduinoCore-samd\cores\arduino\avr\dtostrf.c

adafruit-arduinoCore-samd\cores\arduino\cortex_handlers.c

adafruit-arduinoCore-samd\cores\arduino\delay.c

adafruit-arduinoCore-samd\cores\arduino\hooks.c

adafruit-arduinoCore-samd\cores\arduino\IPAddress.cpp

adafruit-arduinoCore-samd\cores\arduino\itoa.c

adafruit-arduinoCore-samd\cores\arduino\main.cpp

adafruit-arduinoCore-samd\cores\arduino\math_helper.c

adafruit-arduinoCore-samd\cores\arduino\new.cpp

adafruit-arduinoCore-samd\cores\arduino\Print.cpp

adafruit-arduinoCore-samd\cores\arduino\pulse.c

adafruit-arduinoCore-samd\cores\arduino\Reset.cpp

adafruit-arduinoCore-samd\cores\arduino\SERCOM.cpp

adafruit-arduinoCore-samd\cores\arduino\startup.c

adafruit-arduinoCore-samd\cores\arduino\Stream.cpp

adafruit-arduinoCore-samd\cores\arduino\TinyUSB\Adafruit_TinyUSB_SAMD.cpp

adafruit-arduinoCore-samd\cores\arduino\Tone.cpp

adafruit-arduinoCore-samd\cores\arduino\Uart.cpp

adafruit-arduinoCore-samd\cores\arduino\USB\CDC.cpp

adafruit-arduinoCore-samd\cores\arduino\USB\PluggableUSB.cpp

adafruit-arduinoCore-samd\cores\arduino\USB\samd21_host.c

adafruit-arduinoCore-samd\cores\arduino\USB\SAMD21_USBDevice.cpp

adafruit-arduinoCore-samd\cores\arduino\USB\USBCore.cpp

adafruit-arduinoCore-samd\cores\arduino\WInterrupts.c

adafruit-arduinoCore-samd\cores\arduino\wiring.c

adafruit-arduinoCore-samd\cores\arduino\wiring_analog.c

adafruit-arduinoCore-samd\cores\arduino\wiring_digital.c

adafruit-arduinoCore-samd\cores\arduino\wiring_private.c

adafruit-arduinoCore-samd\cores\arduino\wiring_shift.c

adafruit-arduinoCore-samd\cores\arduino\WMath.cpp

adafruit-arduinoCore-samd\cores\arduino\WString.cpp

adafruit-arduinoCore-samd\libraries\Adafruit_NeoPixel\Adafruit_NeoPixel.cpp

adafruit-arduinoCore-samd\libraries\Adafruit_ZeroDMA\Adafruit_ZeroDMA.cpp

adafruit-arduinoCore-samd\libraries\SPI\SPI.cpp

adafruit-arduinoCore-samd\libraries\Wire\Wire.cpp

Adafruit_ZeroTimer.cpp

BLE_Scaner.cpp

checkPushButton.cpp

check_IGN.cpp

check_PIR.cpp

LED_Indicator.cpp

general_varible.cpp

pinout\variant.cpp

sketch.cpp

sound_Indicator.cpp

writeReadFromSpace.cpp

