/*
 * CPPFile1.cpp
 *
 * Created: 25/04/2021 6:17:39 PM
 *  Author: pc
 */ 

#pragma once

#include "Adafruit_ZeroTimer.h"

#include "pinout/gpio-pin-map.h"


#define SIGNALS_COUNT (5)
#define INDICATOR_SIGNAL_MAXIMUM_LENGTH (3600000)

class PwmBuzzer 
{
	public:
	
	PwmBuzzer(int pin, int timerNum) : zt(timerNum), pin(pin)
	{
		indicatorTimer = 0;
		for (int i = 0; i < SIGNALS_COUNT; i++)
		{
			signals[i].length = lengthInvalid;
		}
		
		zt.configure(TC_CLOCK_PRESCALER_DIV1, // prescaler
		TC_COUNTER_SIZE_16BIT,   // bit width of timer/counter
		TC_WAVE_GENERATION_MATCH_PWM // frequency or PWM mode
		);

		zt.PWMout(true, 1, pin);
		zt.enable(false);
		
		off();
	}
	
	void start(uint16_t Length, uint16_t period_on = 1, uint16_t period_off = 0, uint16_t frequency = 0)
	{
		
		indicatorTimer = 0;
		
		signal_t s;
		s.length = Length;
		s.periodOn = period_on;
		s.periodOff = period_off;
		s.freq = frequency;
		s.timer = 0;
		
		if (s.length == 0)
		{
			//continuous signal. override any existing continuous signals
			for (int i = 0; i < SIGNALS_COUNT; i++)
			{
				if (0 == signals[i].length)
				{
					//found empty signal slot
					s.timer = signals[i].timer;
					signals[i] = s;
					return;
				}
			}
		}

		for (int i = 0; i < SIGNALS_COUNT; i++)
		{
			if (lengthInvalid == signals[i].length)
			{
				//found empty signal slot
				signals[i] = s;
				return;
			}
		}
	}
	
	bool active()
	{
		for (int i = 0; i < SIGNALS_COUNT; i++)
		{
			if (lengthInvalid != signals[i].length)
			{
				return true;
			}
		}
		return false;
	}
	
	
	void stop() //stop continuous signals with indefinite length
	{
		off();
		for (int i = 0; i < SIGNALS_COUNT; i++)
		{
			if (signals[i].length == 0)
			{
				signals[i].length = lengthInvalid;
			}
		}
	}
	
	void stopAll() //stop all signals
	{
		off();
		for (int i = 0; i < SIGNALS_COUNT; i++)
		{
			signals[i].length = lengthInvalid;

		}
	}
	
	stopwatch tt = 0;
	void loop()
	{
		uint8_t min_i = uint8_t(-1); //the index of signal with lowest length
		for (int i = 0; i < SIGNALS_COUNT; i++)
		{
			//search for signal with lowest length and process it first
			if (signals[i].length != lengthInvalid)
			{
				if (signals[i].timer > signals[i].length && signals[i].length != 0)
				{
					//signal finished
					signals[i].length = lengthInvalid;
					//DBG("signal end");
				}
				else
				{
					if (min_i != uint8_t(-1))
					{
						if (((int32_t) signals[i].length - (int32_t) signals[i].timer) < ((int32_t) signals[min_i].length - (int32_t) signals[min_i].timer) && signals[i].length != 0)
						{
							min_i = i;
						}
					}
					else
					{
						if (signals[i].length != 0)
						{
							min_i = i;
						}
					}
				}
			}
		}
		
		if (min_i == uint8_t(-1))
		{
			for (int i = 0; i < SIGNALS_COUNT; i++)
			{
				if (signals[i].length == 0)
				{
					min_i = i;
				}
			}
			
		}
		
		if (min_i == uint8_t(-1) || indicatorTimer > INDICATOR_SIGNAL_MAXIMUM_LENGTH)
		{
			off();
			return; //no signal pending
		}

		signal_t s = signals[min_i];
		//DBG_("indicator ") DBG_(pin.pin) DBG_(" ") DBG((s.timer % (s.periodOn+s.periodOff)) <= s.periodOn)
		//DBG_("indicator ") DBG_(s.timer) DBG_(" ") DBG_(s.periodOn) DBG_(" ") DBG_(s.periodOff) DBG_(" ") DBG(s.timer % (s.periodOn+s.periodOff))
		put((s.timer % (s.periodOn+s.periodOff)) < s.periodOn);
	}
	
	private:
	Adafruit_ZeroTimer zt;
	const int pin = -1;
	stopwatch indicatorTimer = 0;
	static const uint16_t lengthInvalid = uint16_t(-1);
	struct signal_t
	{
		uint16_t length = lengthInvalid; //length lengthInvalid means nothing is output. length 0 means continuous output
		uint16_t periodOn = 1;
		uint16_t periodOff = 0;
		uint16_t freq = 0;
		stopwatch timer = 0;
	};
	
	signal_t signals[SIGNALS_COUNT];
	
	static const uint32_t tonePeriod_default = 20000;
	
	void on(uint32_t tonePeriod = tonePeriod_default);
	void put(bool state);
	void off();
	
	bool state = false;
	
	};

extern PwmBuzzer buzzer;
	
#if __INCLUDE_LEVEL__ == 0

PwmBuzzer buzzer(BuzzerControl, 3);

void PwmBuzzer::off()
{
	if (!state) return;
	state = false;
	
	zt.enable(false);
	
	pinMode(pin, OUTPUT);
	digitalWrite(pin, HIGH);
}

void PwmBuzzer::on(uint32_t tonePeriod)
{
	if (state) return;
	state = true;
	
	zt.setPeriodMatch(tonePeriod, tonePeriod/2);
	zt.PWMout(true, 1, pin);
	zt.enable(true);
}

void PwmBuzzer::put(bool state)
{
	if (!state) off(); else on(tonePeriod_default);
}

#endif

